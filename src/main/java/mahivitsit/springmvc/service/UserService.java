package mahivitsit.springmvc.service;

import mahivitsit.springmvc.model.Login;
import mahivitsit.springmvc.model.User;
public interface UserService {

	void register(User user);

	User validateUser(Login login);
}
