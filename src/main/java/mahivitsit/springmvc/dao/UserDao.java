package mahivitsit.springmvc.dao;

import mahivitsit.springmvc.model.Login;

import mahivitsit.springmvc.model.User;
public interface UserDao {
	  void register(User user);

	  User validateUser(Login login);
}
