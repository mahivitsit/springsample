package mahivitsit.springmvc.service;
import org.springframework.beans.factory.annotation.Autowired;

import mahivitsit.springmvc.dao.UserDao;
import mahivitsit.springmvc.model.Login;
import mahivitsit.springmvc.model.User;
public class UserServiceImpl
{
	@Autowired
	  public UserDao userDao;

	  public void register(User user)
	  {
	    userDao.register(user);
	  }

	  public User validateUser(Login login) {
	    return userDao.validateUser(login);
	}
}
